# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

SRCS_DIR=./src
INSTALL_DIR=/usr/local/
TARGET_DIR=$(SRCS_DIR)/target
export PROJECT_NAME=main

all:
	$(MAKE) -C $(SRCS_DIR)

install:
	cp $(TARGET_DIR)/$(PROJECT_NAME) $(INSTALL_DIR)bin/$(PROJECT_NAME)

clean:
	$(MAKE) -C $(SRCS_DIR) clean
