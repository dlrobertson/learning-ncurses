/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SLACK_CONTENT_H
#define SLACK_CONTENT_H

#include <pthread.h>

typedef struct struct_listItem {
    char* usr;
    char* msg;
    int   len;
} ListItem;

typedef struct struct_listNode {
    ListItem* data;
    struct struct_listNode* prev;
    struct struct_listNode* next;
} ListNode;

typedef struct struct_content {
    pthread_mutex_t* mut;
    pthread_cond_t*  cv;
    ListNode*        tail;
} Content;

ListItem* listItem_create(const char*, const char*);

void listItem_destroy(ListItem*);

void listNode_append(ListNode*, const char*, const char*);

ListNode* listNode_init();

ListNode* listNode_create(ListItem*, ListNode*);

ListNode* listNode_create_new(const char*, const char*, ListNode*);

void listNode_destroy();

void listNode_append(ListNode*, const char*, const char*);

void listNode_append_item(ListNode*, ListItem*);

Content* create_content(ListNode*);

Content* create_new_content();

void destroy_content(Content*);

#endif
