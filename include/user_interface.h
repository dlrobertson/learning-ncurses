/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SLACK_USER_INTERFACE_H
#define SLACK_USER_INTERFACE_H

#include <pthread.h>

#include "btree.h"

#define PANE_SIZE 20

typedef struct struct_side_pane {
    WINDOW* win;
} SidePane;

typedef struct struct_ui {
    SidePane* side_pane;
    WinNode*  head; // head of btree
    WinNode*  curr; // must be a leaf
} UserInterface;

SidePane* create_side_pane(int, int, int, int);

void clear_side_pane(SidePane*);

void destroy_side_pane(SidePane*);

void ui_get_input(UserInterface*);

int ui_process_input(UserInterface*, const char*);

int ui_process_command(UserInterface*, const char*);

#endif
