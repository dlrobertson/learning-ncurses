/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SLACK_MAIN_H
#define SLACK_MAIN_H

static void startup_ui() __attribute__((constructor(100)));

static void cleanup_ui() __attribute__((destructor(100)));

#endif
