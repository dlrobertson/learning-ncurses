/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SLACK_BTREE_H
#define SLACK_BTREE_H

#include <ncurses.h>

#include "content.h"

#define MIN_COMMENT_BOX_HEIGHT 3
#define MIN_COMMENT_BOX_WIDTH 6

typedef enum {
    LEAF,
    INTERNAL
} NodeType;

typedef enum {
    VERTICAL,
    HORIZONTAL,
    NONE
} SplitType;

typedef struct struct_cur {
    int x;
    int y;
} Cur;

typedef struct struct_window {
    Content* content; // content is _not_ owned
    Cur      cursor;
    WINDOW*  area;
    WINDOW*  input;
} Win;

typedef struct struct_win_metadata {
    NodeType  type;
    SplitType split;
    int       height;
    int       width;
    int       y;
    int       x;
} WinMetadata;

typedef struct struct_winnode {
    WinMetadata*           metadata;
    struct struct_winnode* parent;
    union {
        struct {
            struct struct_winnode* lhs;
            struct struct_winnode* rhs;
        } internal;
        Win* win;
    } data;
} WinNode;

// Win functions
Win* create_win(int ht, int wd, int y, int x);

void clear_win(Win* pwin);

void destroy_win(Win* pwin);

void redraw_win(Win* pwin, int ht, int wd, int y, int x);

void win_add_char(Win*, int);

void win_del_char(Win*);

void win_reset_cursor(Win*);

// WinMetadata functions
WinMetadata* create_winmetadata(NodeType type, SplitType split,
                             int ht, int wd, int y, int x);

void destroy_winmetadata(WinMetadata* metadata);

// WinNode functions
void winnode_set_rhs(WinNode* node, WinNode* rhs);

void winnode_set_lhs(WinNode* node, WinNode* lhs);

void winnode_set_leaves(WinNode* node, WinNode* lhs, WinNode* rhs);

WinNode* winnode_get_rhs(WinNode* node);

WinNode* winnode_get_lhs(WinNode* node);

Win* winnode_get_win(WinNode*);

WinNode* create_internal_winnode(int ht, int wd, int y, int x, WinNode* parent,
                                 SplitType split, WinNode* lhs, WinNode* rhs);

void winnode_redraw(WinNode* node);

WinNode* create_leaf_winnode(int ht, int wd, int y, int x, WinNode* parent);

WinNode* winnode_other_leaf(WinNode* node);

void winnode_steal_data(WinNode* first, WinNode* second);

void destroy_winnode(WinNode* node);

WinNode* winnode_trim_destroy(WinNode* node);

WinNode* winnode_hsplit_leaf(WinNode* node);

WinNode* winnode_vsplit_leaf(WinNode* node);

int winnode_is_inside(WinNode* node, int y, int x);

int winnode_is_head(WinNode* node);

WinNode* winnode_find_leaf(WinNode* node, int y, int x);

#endif
