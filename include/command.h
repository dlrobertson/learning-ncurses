#ifndef SLACK_COMMAND_H
#define SLACK_COMMAND_H

#include <user_interface.h>

#define NCOMMANDS 9
#define NMIDCMD (NCOMMANDS/2)

typedef int(*Action)(UserInterface*);

typedef struct struct_command {
    const char cmd[10];
    int(*action)(UserInterface*);
} Command;

int cmd_exit(UserInterface*);

int cmd_hsplit(UserInterface*);

int cmd_quit(UserInterface*);

int cmd_vsplit(UserInterface*);

int cmd_nop(UserInterface*);

int cmd_up(UserInterface*);

int cmd_down(UserInterface*);

int cmd_left(UserInterface*);

int cmd_right(UserInterface*);

int cmd_not_found(UserInterface*);

static Command s_commands[NCOMMANDS] = {
  {"/down", &cmd_down},
  {"/exit", &cmd_exit},
  {"/hsplit", &cmd_hsplit},
  {"/left", &cmd_left},
  {"/nop", &cmd_nop},
  {"/quit", &cmd_quit},
  {"/right", &cmd_right},
  {"/up", &cmd_up},
  {"/vsplit", &cmd_vsplit}
};

Action find_cmd(const char*);

#endif
