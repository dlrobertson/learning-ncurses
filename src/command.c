#include <string.h>
#include <stdio.h>

#include <command.h>

int cmd_exit(UserInterface* iface) {
    return 0;
}

int cmd_nop(UserInterface* iface) {
    return 1;
}

int cmd_hsplit(UserInterface* iface) {
    iface->curr = winnode_hsplit_leaf(iface->curr);
    return 1;
}

int cmd_vsplit(UserInterface* iface) {
    iface->curr = winnode_vsplit_leaf(iface->curr);
    return 1;
}

int cmd_quit(UserInterface* iface) {
    if(winnode_is_head(iface->curr)) {
        return cmd_exit(iface);
    }
    WinMetadata metadata = *(iface->curr->metadata);
    winnode_trim_destroy(iface->curr);
    iface->curr = winnode_find_leaf(iface->head, metadata.y, metadata.x);
    winnode_redraw(iface->head);
    return 1;
}

int cmd_up(UserInterface* iface) {
    WinMetadata metadata = *(iface->curr->metadata);
    if(metadata.y > 0) {
        WinNode* up = winnode_find_leaf(iface->head, metadata.y - 1, metadata.x);
        if(up) {
            iface->curr = up;
            winnode_redraw(iface->head);
        }
    }
    return 1;
}

int cmd_down(UserInterface* iface) {
    WinMetadata metadata = *(iface->curr->metadata);
    int last_line = metadata.y + metadata.height + 1;
    if(last_line < LINES) {
        WinNode* down = winnode_find_leaf(iface->head, last_line,
                                        metadata.x);
        if(down) {
            iface->curr = down;
            winnode_redraw(iface->head);
        }
    }
    return 1;
}

int cmd_left(UserInterface* iface) {
    WinMetadata metadata = *(iface->curr->metadata);
    if(metadata.x > 0) {
        WinNode* left = winnode_find_leaf(iface->head, metadata.y, metadata.x - 1);
        if(left) {
            iface->curr = left;
            winnode_redraw(iface->head);
        }
    }
    return 1;
}

int cmd_right(UserInterface* iface) {
    WinMetadata metadata = *(iface->curr->metadata);
    int last_col = metadata.x + metadata.width + 1;
    if(last_col < COLS) {
        WinNode* right = winnode_find_leaf(iface->head, metadata.y, last_col);
        if(right) {
            iface->curr = right;
            winnode_redraw(iface->head);
        }
    }
    return 1;
}

int cmd_not_found(UserInterface* iface) {
    return 1;
}

Action binary_search(const char* cmd, Command* mid, Command* end) {
    if(!(mid < end) || mid < s_commands) {
        return 0;
    }
    long pos = strcmp(cmd, mid->cmd);
    if(pos < 0) {
        return binary_search(cmd, mid - ((mid - s_commands)/2) - 1,
                             mid);
    } else if(pos > 0) {
        if(mid == end - 1) {
            return 0;
        }
        return binary_search(cmd, mid + ((end - mid)/2), end);
    } else {
        return mid->action;
    }
}

Action find_cmd(const char* cmd) {
    Command* midcmd = (s_commands + NMIDCMD);
    Command* endcmd = (s_commands + NCOMMANDS);
    Action act = binary_search(cmd, midcmd, endcmd);
    if(act) {
        return act;
    } else {
        return &cmd_not_found;
    }
}
