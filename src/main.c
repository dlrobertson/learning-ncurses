/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <stdlib.h>
#include <content.h>
#include <btree.h>
#include <command.h>
#include <user_interface.h>
#include <main.h>

static UserInterface* ui = NULL;

static void startup_ui() {
    ui = (UserInterface*)malloc(sizeof(UserInterface));

    initscr();
    cbreak();
    raw();
    noecho();
    keypad(stdscr, TRUE);
    if(has_colors() == FALSE) {
        endwin();
        printf("Your terminal does not support color\n");
        exit(-1);
    }
    start_color();
    init_pair(1, COLOR_WHITE, COLOR_BLACK);
    init_pair(2, COLOR_GREEN, COLOR_YELLOW);
    init_pair(3, COLOR_BLACK, COLOR_RED);
    refresh();
    SidePane* pane = create_side_pane(LINES, PANE_SIZE, 0, 0);
    WinNode* head = create_leaf_winnode(LINES, COLS - PANE_SIZE,
                                        0, PANE_SIZE, NULL);
    ui->side_pane = pane;
    ui->head = head;
    ui->curr = head;
}

static void cleanup_ui() {
    if(ui) {
        destroy_winnode(ui->head);
        destroy_side_pane(ui->side_pane);
        free(ui);
        endwin();
    }
}

int main(int argc, char** argv) {
    ui_get_input(ui);
    //ui_process_input(ui, "/hsplit");
    //ui_process_input(ui, "/vsplit");
    //ui_process_input(ui, "/hsplit");
    //ui_process_input(ui, "/vsplit");
    //ui_process_input(ui, "/quit");
    //ui_process_input(ui, "/quit");
    //ui_process_input(ui, "/quit");
    //ui_process_input(ui, "/quit");
    //ui_process_input(ui, "/exit");

    return 0;
}
