/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <stdlib.h>
#include <string.h>

#include <user_interface.h>
#include <content.h>
#include <command.h>

SidePane* create_side_pane(int ht, int wd, int y, int x) {
    if(MIN_COMMENT_BOX_HEIGHT < ht && MIN_COMMENT_BOX_HEIGHT < wd) {
        WINDOW* area = newwin(ht, wd, y, x);
        box(area, 0, 0);

        wrefresh(area);

        SidePane* pane = (SidePane*)malloc(sizeof(SidePane));
        pane->win = area;
        return pane;
    }

    return 0;
}

void clear_side_pane(SidePane* pane) {
    wborder(pane->win, ' ', ' ', ' ',' ',' ',' ',' ',' ');
    wrefresh(pane->win);
    delwin(pane->win);
    return;
}

void destroy_side_pane(SidePane* pane) {
    if(pane) {
        clear_side_pane(pane);
        free(pane);
        pane = 0;
    }
    return;
}

void ui_get_input(UserInterface* iface) {
    int ch;
    int bExit = 1;
    while(bExit) {
        int i = 0;
        char cmd[255];
        WinNode* curr = iface->curr;
        Win* curr_win = winnode_get_win(curr);
        win_reset_cursor(curr_win);
        winnode_redraw(curr);
        while((ch = getch()) != 10 && i < 254) {
            if(ch > 0x1f && ch < 0x7f && curr_win->cursor.x < curr->metadata->width) {
                cmd[i] = ch;
                ++i;
                win_add_char(curr_win, ch);
            } else if(ch == 0x7f && curr_win->cursor.x > 0) {
                --i;
                cmd[i] = 0x20;
                win_del_char(curr_win);
            }
        }
        if(strlen(cmd) > 0) {
            cmd[i] = '\0';
            bExit = ui_process_input(iface, cmd);
        }
    }
}

int ui_process_input(UserInterface* iface, const char* cmd) {
    if(cmd[0] == '/') {
        return ui_process_command(iface, cmd);
    } else {
        // handle input text
    }
    return 1;
}

int ui_process_command(UserInterface* iface, const char* cmd) {
    Action action = find_cmd(cmd);
    return (*action)(iface);
}
