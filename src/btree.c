/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <stdlib.h>
#include <stdio.h>

#include <ncurses.h>
#include <btree.h>
#include <content.h>

#define INPUT_COLOR COLOR_PAIR(1)

Win* create_win(int ht, int wd, int y, int x) {
    if(MIN_COMMENT_BOX_HEIGHT < ht && MIN_COMMENT_BOX_HEIGHT < wd) {
        int new_height = ht - 2;
        int new_width = wd - 2;
        WINDOW* area = newwin(ht, wd, y, x);
        WINDOW* input = newwin(1, new_width, y + new_height, x + 1);

        wbkgdset(input, INPUT_COLOR);
        wrefresh(input);

        int col;
        --new_width;
        for(col = 0; col < new_width; col += 2) {
            mvwprintw(input, 0, col, " ");
            mvwprintw(input, 0, col + 1, " ");
        }
        for(;col < new_width+1; ++col) {
            mvwprintw(input, 0, col, " ");
        }

        box(area, 0, 0);

        wrefresh(area);
        wrefresh(input);

        Win* pwin = (Win*)malloc(sizeof(Win));
        pwin->area = area;
        pwin->input = input;
        pwin->cursor.x = 0;
        pwin->cursor.y = 0;
        wmove(input, 0, 0);
        return pwin;
    }

    return 0;
}

void clear_win(Win* pwin) {
    wborder(pwin->area, ' ', ' ', ' ',' ',' ',' ',' ',' ');
    wrefresh(pwin->area);
    delwin(pwin->area);
    wborder(pwin->input, ' ', ' ', ' ',' ',' ',' ',' ',' ');
    wrefresh(pwin->input);
    delwin(pwin->input);
    return;
}

void destroy_win(Win* pwin) {
    if(pwin) {
        clear_win(pwin);
        free(pwin);
    }
    return;
}

void win_refresh(Win* pwin) {
    wrefresh(pwin->area);
    wrefresh(pwin->input);
}

void redraw_win(Win* pwin, int ht, int wd, int y, int x) {
    if(!pwin) {
        return;
    }
    if(MIN_COMMENT_BOX_HEIGHT < ht && MIN_COMMENT_BOX_HEIGHT < wd) {
        clear_win(pwin);
        int new_height = ht - 2;
        int new_width = wd - 2;
        WINDOW* area = newwin(ht, wd, y, x);
        WINDOW* input = newwin(1, new_width, y + new_height, x + 1);

        wbkgdset(input, INPUT_COLOR);
        wrefresh(input);

        int col;
        --new_width;
        for(col = 0; col < new_width; col += 2) {
            mvwprintw(input, 0, col, " ");
            mvwprintw(input, 0, col + 1, " ");
        }
        for(;col < new_width+1; ++col) {
            mvwprintw(input, 0, col, " ");
        }

        box(area, 0, 0);

        wrefresh(area);
        wrefresh(input);
        pwin->area = area;
        pwin->input = input;
    }
    return;
}

void win_add_char(Win* pwin, int ch) {
    Cur cursor = pwin->cursor;
    WINDOW* input = pwin->input;
    wmove(input, cursor.y, cursor.x);
    waddch(input, ch);
    ++pwin->cursor.x;
    wrefresh(input);
    return;
}

void win_del_char(Win* pwin) {
    WINDOW* input = pwin->input;
    --pwin->cursor.x;
    Cur cursor = pwin->cursor;
    mvwaddch(input, cursor.y, cursor.x, 0x20);
    wmove(input, cursor.y, cursor.x);
    wrefresh(input);
    return;
}

void win_reset_cursor(Win* pwin) {
    pwin->cursor.x = 0;
    pwin->cursor.y = 0;
    wmove(pwin->input, pwin->cursor.y, pwin->cursor.x);
    wrefresh(pwin->input);
    return;
}

WinMetadata* create_winmetadata(NodeType type, SplitType split,
                             int ht, int wd, int y, int x) {
    WinMetadata* metadata = (WinMetadata*)malloc(sizeof(WinMetadata));
    metadata->type        = type;
    metadata->split       = split;
    metadata->height      = ht;
    metadata->width       = wd;
    metadata->y           = y;
    metadata->x           = x;

    return metadata;
}

void destroy_winmetadata(WinMetadata* metadata) {
    if(metadata) {
        free(metadata);
    }
    return;
}

void winnode_set_rhs(WinNode* node, WinNode* rhs) {
    node->data.internal.rhs = rhs;
    return;
}

void winnode_set_lhs(WinNode* node, WinNode* lhs) {
    node->data.internal.lhs = lhs;
    return;
}

void winnode_set_leaves(WinNode* node, WinNode* lhs, WinNode* rhs) {
    node->data.internal.rhs = rhs;
    node->data.internal.lhs = lhs;
    return;
}

WinNode* winnode_get_rhs(WinNode* node) {
    if(node && node->metadata->type == INTERNAL) {
        return node->data.internal.rhs;
    }
    return 0;
}

WinNode* winnode_get_lhs(WinNode* node) {
    if(node && node->metadata->type == INTERNAL) {
        return node->data.internal.lhs;
    }
    return 0;
}

Win* winnode_get_win(WinNode* node) {
    if(node && node->metadata->type == LEAF) {
        return node->data.win;
    }
    return 0;
}

WinNode* create_internal_winnode(int ht, int wd, int y, int x, WinNode* parent,
                                 SplitType split, WinNode* lhs, WinNode* rhs) {
    WinNode* node           = (WinNode*)malloc(sizeof(WinNode));
    node->metadata          = create_winmetadata(INTERNAL, split, ht, wd, y, x);
    node->parent            = parent;
    node->data.internal.lhs = lhs;
    node->data.internal.rhs = rhs;

    return node;
}

void winnode_redraw(WinNode* node) {
    if(node && node->metadata) {
        Win* pwin;
        WinMetadata meta = *node->metadata;
        switch(meta.type) {
            case LEAF:
                pwin = node->data.win;
                redraw_win(pwin, meta.height, meta.width, meta.y, meta.x);
                break;
            case INTERNAL:
                winnode_redraw(winnode_get_lhs(node));
                winnode_redraw(winnode_get_lhs(node));
        }
    }
}

WinNode* create_leaf_winnode(int ht, int wd, int y, int x, WinNode* parent) {
    WinNode* node  = (WinNode*)malloc(sizeof(WinNode));
    node->metadata = create_winmetadata(LEAF, NONE, ht, wd, y, x);
    node->parent   = parent;
    node->data.win = create_win(ht, wd, y, x);

    return node;
}

// No checking done to ensure there is another leaf
WinNode* winnode_other_leaf(WinNode* node) {
    WinNode parent = *node->parent;
    WinNode* other = (WinNode*)(
      (
        (
          (unsigned long) parent.data.internal.lhs ^
          (unsigned long) parent.data.internal.rhs
          )
        ) ^ (unsigned long)node
      );
    return other;
}

// No checking is done to see if the data from first has
// been freed
void winnode_steal_data(WinNode* first, WinNode* second) {
    NodeType type          = second->metadata->type;
    first->metadata->type  = type;
    first->metadata->split = second->metadata->split;
    first->data            = second->data;
    switch(type) {
        case LEAF:
            second->data.win = 0;
            break;
        case INTERNAL:
            second->data.internal.lhs = 0;
            second->data.internal.rhs = 0;
    }
    destroy_winmetadata(second->metadata);
    free(second);
}

void destroy_winnode(WinNode* node) {
    if(node) {
        switch(node->metadata->type) {
            case LEAF:
                destroy_winmetadata(node->metadata);
                destroy_win(node->data.win);
                free(node);
                break;
            case INTERNAL:
                destroy_winnode(node->data.internal.lhs);
                destroy_winnode(node->data.internal.rhs);
                destroy_winmetadata(node->metadata);
                free(node);
                break;
        }
    }
    return;
}

WinNode* winnode_trim_destroy(WinNode* node) {
    if(node && node->parent) {
        WinNode* other = winnode_other_leaf(node);
        WinNode* parent = node->parent;

        destroy_winnode(node);

        winnode_steal_data(parent, other);
        winnode_redraw(parent);
        return parent;
    } else {
        destroy_winnode(node);
    }
    return 0;
}

WinNode* winnode_hsplit_leaf(WinNode* node) {
    if(node && node->metadata) {
        WinMetadata* meta = node->metadata;
        if(meta->type == LEAF) {
            destroy_win(node->data.win);

            meta->type = INTERNAL;
            meta->split = HORIZONTAL;
            int new_height = meta->height/2;
            WinNode* bot = create_leaf_winnode(new_height, meta->width,
                                               meta->y, meta->x, node);
            WinNode* top = create_leaf_winnode(meta->height - new_height, meta->width,
                                               meta->y + new_height, meta->x, node);
            winnode_set_rhs(node, top);
            winnode_set_lhs(node, bot);
            return top;
        }
    }
    return node;
}

WinNode* winnode_vsplit_leaf(WinNode* node) {
    if(node && node->metadata) {
        WinMetadata* meta = node->metadata;
        if(meta->type == LEAF) {
            destroy_win(node->data.win);

            meta->type = INTERNAL;
            meta->split = VERTICAL;
            int new_width = meta->width/2;
            WinNode* bot = create_leaf_winnode(meta->height, new_width,
                                               meta->y, meta->x, node);
            WinNode* top = create_leaf_winnode(meta->height, meta->width - new_width,
                                               meta->y, meta->x + new_width, node);
            winnode_set_rhs(node, top);
            winnode_set_lhs(node, bot);
            return top;
        }
    }
    return 0;
}

int winnode_is_inside(WinNode* node, int y, int x) {
    WinMetadata meta = *node->metadata;
    if(
        y >= meta.y && x >= meta.x &&
        x <= (meta.x + meta.width) && y <= (meta.y + meta.height)
      ) {
        return 1;
    }
    return 0;
}

int winnode_is_head(WinNode* node) {
    return (node->parent == 0)? 1 : 0;
}

WinNode* winnode_find_leaf(WinNode* node, int y, int x) {
    WinMetadata meta = *node->metadata;
    if(meta.type == LEAF) {
        return node;
    }
    WinNode* lhs = winnode_get_lhs(node);
    WinNode* rhs = winnode_get_rhs(node);
    if(winnode_is_inside(lhs, y, x)) {
        return winnode_find_leaf(lhs, y, x);
    } else {
        return winnode_find_leaf(rhs, y, x);
    }
    return 0;
}

