/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <string.h>
#include <stdlib.h>

#include <content.h>

ListItem* listItem_create(const char* usr, const char* msg) {
    ListItem* item = (ListItem*)malloc(sizeof(ListItem));
    item->usr = strdup(usr);
    item->msg = strdup(msg);
    item->len = strlen(msg);
    return item;
}

void listItem_destroy(ListItem* item) {
    if(item) {
        free(item->usr);
        free(item->msg);
        free(item);
    }
    return;
}

ListNode* listNode_init() {
    ListNode* tail = (ListNode*)malloc(sizeof(ListNode));
    tail->prev = 0;
    tail->next = 0;
    tail->data = listItem_create("", "Welcome to my app");
    return tail;
}

ListNode* listNode_create(ListItem* item, ListNode* prev) {
    ListNode* node = (ListNode*)malloc(sizeof(ListNode));
    node->data = item;
    node->prev = prev;
    node->next = 0;
    prev->next = node;

    return node;
}

ListNode* listNode_create_new(const char* usr, const char* msg, ListNode* prev) {
    ListNode* node = (ListNode*)malloc(sizeof(ListNode));
    ListItem* item = listItem_create(usr, msg);
    node->data = item;
    node->prev = prev;
    node->next = 0;
    prev->next = node;

    return node;
}

void listNode_destroy(ListNode* tail) {
    if(tail) {
        ListNode* prev = tail->prev;
        listItem_destroy(tail->data);
        free(tail);
        tail = prev;
    }
    return;
}

void listNode_append(ListNode* tail, const char* usr, const char* msg) {
    if(tail && !tail->next) {
        tail = listNode_create_new(usr, msg, tail);
    }
    return;
}

void listNode_append_item(ListNode* tail, ListItem* item) {
    if(tail && !tail->next) {
        tail = listNode_create(item, tail);
    }
    return;
}

Content* create_content(ListNode* tail) {
    Content* tmp = (Content*)malloc(sizeof(Content));
    pthread_cond_init(tmp->cv, NULL);
    pthread_mutex_init(tmp->mut, NULL);
    tmp->tail = tail;
    return tmp;
}

Content* create_new_content() {
    Content* tmp = (Content*)malloc(sizeof(Content));
    pthread_cond_init(tmp->cv, NULL);
    pthread_mutex_init(tmp->mut, NULL);
    tmp->tail = listNode_init();
    return tmp;
}

void destroy_content(Content* ctnt) {
    if(ctnt) {
        pthread_mutex_destroy(ctnt->mut);
        pthread_cond_destroy(ctnt->cv);
        ctnt = 0;
    }
    return;
}
